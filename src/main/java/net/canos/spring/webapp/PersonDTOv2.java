package net.canos.spring.webapp;

import java.util.Date;


public class PersonDTOv2 {
	public enum Gender {MALE, FEMALE};
	
	private Integer id;
	private String name;
	private String lastName;
	private Date birthday;
	private Integer height;
	private Integer weight; 
	private String gender; 
	
	public PersonDTOv2() {
		super();
	}
	
	public PersonDTOv2(Person p) {
		this.id = p.getId();
		this.name = p.getName();
		this.lastName = p.getSurName();
		this.birthday = p.getBirthday();
		this.height = p.getHeight();
		this.weight = p.getWeight();
		this.gender = (p.getGender() != null && p.getGender().equals(Gender.MALE)) ? "male" : "female";
	}
	
	public PersonDTOv2(PersonForm p) {
		this.id = p.getId();
		this.name = p.getName();
		this.lastName = p.getSurName();
		this.birthday = p.getBirthday();
		this.height = p.getHeight();
		this.weight = p.getWeight();
		this.gender = (p.getGender() != null && p.getGender().equals(Gender.MALE)) ? "male" : "female";
	}
	public PersonDTOv2(PersonDTO p) {
		this.id = p.getId();
		this.name = p.getName();
		this.lastName = p.getSurName();
		this.birthday = p.getBirthday();
		this.height = p.getHeight();
		this.weight = p.getWeight();
		this.gender = (p.getGender() != null && p.getGender().equals(Gender.MALE)) ? "male" : "female";
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String surName) {
		this.lastName = surName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	
	@Override
	public String toString() {
		return "Person [name=" + name + ", surName=" + lastName + ", birthday=" + birthday + ", height=" + height
				+ ", weight=" + weight + "]";
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
